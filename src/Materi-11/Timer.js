import React, {Component} from "react"

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 0,
      showTime: true,
      buttonStopVisible: true
    }
  }

  tick() {
    this.setState({
      time: this.state.time + 1 
    });
  }

  componentDidMount(){
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentDidUpdate(){
    if (this.state.buttonStopVisible === true){
      if (this.state.time > 5){
        this.setState({
          buttonStopVisible : false
        })
      }
    }
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  stopTheTimer = ()=>{
    this.componentWillUnmount()
  }

  deleteTheTimer = ()=>{
    this.setState({
      showTime: false
    })
  }


  render(){
    return(
      <>
        {this.state.showTime && (
          <>
            <h1 style={{textAlign: "center"}}>
              {this.state.time}
            </h1>
          </>
        )}
        {this.state.buttonStopVisible && 
          <button onClick={this.stopTheTimer}>Stop Time</button>
        }
        <button onClick={this.deleteTheTimer}>Delete Time</button>
      </>
    )
  }
}

export default Timer