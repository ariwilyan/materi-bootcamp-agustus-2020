import React from 'react';

function Welcome (props){
  return <h1 style={{textAlign: "center"}}>Welcome {props.name}</h1>
}

export default Welcome