import React from "react"
import UserInfo from "./UserInfo"

export default function ListUserInfo(){
  const data = [
    {name: "John", age: 25, gender: "Male"},
    {name: "Sarah", age: 22, gender: "Female"},
    {name: "David", age: 30, gender: "Male"},
    {name: "Kate", age: 27, gender: "Female" }
  ]
  return (
    <>
      <div style={{width: "60%", margin: "0 auto"}}>
        {
          data.map((el, idx)=>{
            return(          
              <UserInfo key={idx} item={el}/>
            )
          })
        }
      </div>
  
    </>
  );
}
