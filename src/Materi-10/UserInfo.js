import React, {Component} from 'react';
import Welcome from "./Welcome"

class UserInfo extends Component{
  render(){
    return (
      <div style={{border: "1px solid #aaa", oorderRadius: "10px", padding: "20px"}}>
        <Welcome name={this.props.item.name} />
        <h1 style={{textAlign: "center"}}>Age : {this.props.item.age}</h1>
        <h1 style={{textAlign: "center"}}>Gender : {this.props.item.gender}</h1>
      </div>
    ) 
  }
}

export default UserInfo